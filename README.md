## Wolfpack Assessment

Original backend assessment:

Welcome to the Backend Wolfpack assessment!
The leader of the pack wants us to create a mobile application to track all the wolves in
the pack. Assume you are creating this app in a team of developers, you are the lead
backend developer of the team. Therefore your task is to design and implement a
RESTful API for this app. The API should be built in such a way that it is easy to
understand for your colleagues and that it can be easily implemented by the app
developers in your team.

This app should have the following functionalities, in the future more functionalities
may be added.

### Wolves
It should be possible to list all the wolves including some basic personal information
such as their name, gender and birthdate. Furthermore it should be possible to add,
remove and update wolves in this list. All wolves have a location and it should be
possible to update the location of a wolf such that we can include a map with all wolves
in the app.

### Packs
Wolves like hanging out in packs, in our app we’d like an option to create packs to which
we can add wolves. Packs consist of a name and one or more wolves. Furthermore,
sometimes we want to remove wolves from a pack again. The app should be able to
display a list of packs and the wolves which are in them.


## Install
- Copy the .env.example to .env
- Add the SQLI path to the env 

or

- Change to MySql
- Remove the connection from "App\Models\Wolf"
- Remove the connection from "App\Models\WolfPack"

#### Postman
I created a Postman collection for you to use.
Please find located at `./_postman`

#### Routes
`php artisan route:list`

#### DB and Seeding
You can refresh the database with
`php artisan migrate:refresh --seed`


## Testing
I created some tests for the API endpoints.
This project was not created as TDD.  
Enter command:
`./vendor/phpunit/phpunit/phpunit`
