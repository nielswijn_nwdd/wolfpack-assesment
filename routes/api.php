<?php

use App\Http\Controllers\Api;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('wolf', Api\WolfController::class);
Route::apiResource('wolfgeopoint', Api\WolfGeoPointController::class)
    ->only(['index', 'show', 'update']);

Route::apiResource('wolfpack', Api\WolfPackController::class);
Route::apiResource('wolfpack.wolves', Api\WolfPackWolvesController::class)
    ->only(['index', 'store', 'destroy']);


Route::fallback(function () {
    return response()->json([
        'message' => 'Not found'
        ],
        404
    );
})->name('api.fallback');
