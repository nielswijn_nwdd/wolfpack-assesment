<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\WolfWolfPacks as WolfPackResource;

class Wolf extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'gender' => $this->gender,
            'email' => $this->email,
            'birthdate' => $this->birthdate,
            'lat' => $this->lat,
            'lon' => $this->lon,
            'createdAt' => (string)$this->created_at,
            'updatedAt' => (string)$this->updated_at,
            'wolfPacks' => WolfPackResource::collection($this->wolfPacks)
        ];
    }
}
