<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Wolf as WolfResource;
use App\Models\Wolf;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Enum;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class WolfController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        return response()->json(WolfResource::collection(Wolf::all()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'firstname' => 'required|max:100',
            'lastname' =>  'required|max:100',
            'gender' =>   'required|in:man,woman,undefined',
            'email' =>  'email|max:255',
            'birthdate' => 'date',
            'lat' => 'numeric',
            'lon' => 'numeric',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $validatedData = $validator->validated();

        $wolf = Wolf::create($validatedData);
        $wolf->save();
        return response()->json(new WolfResource($wolf));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $wolf = Wolf::findOrFail($id);
        return response()->json(new WolfResource($wolf));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Request $request, $id)
    {
        $wolf = Wolf::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'firstname' => 'max:100',
            'lastname' =>  'max:100',
            'gender' =>   'in:man,woman,undefined',
            'email' =>  'email|max:255',
            'birthdate' => 'date',
            'lat' => 'numeric',
            'lon' => 'numeric',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $validatedData = $validator->validated();

        $wolf->update($validatedData);
        return response()->json(new WolfResource($wolf));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse|void
     * @throws \Throwable
     */
    public function destroy($id)
    {
        $wolf = Wolf::findOrFail($id);
        return response()->json(['success' => $wolf->deleteOrFail()]);
    }
}
