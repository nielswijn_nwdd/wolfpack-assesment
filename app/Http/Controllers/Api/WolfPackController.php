<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Wolf;
use App\Models\WolfPack;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Resources\WolfPack as WolfPackResource;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Enum;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class WolfPackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        return response()->json(WolfPackResource::collection(WolfPack::all()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:100',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $validatedData = $validator->validated();

        $wolfPack = WolfPack::create($validatedData);
        $wolfPack->save();
        return response()->json(new WolfPackResource($wolfPack));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $wolfPack = WolfPack::findOrFail($id);
        return response()->json(new WolfPackResource($wolfPack));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Request $request, $id)
    {
        $wolfPack = WolfPack::findOrFail($id);
        if(!$wolfPack instanceof WolfPack){
            throw new NotFoundHttpException();
        }

        $validator = Validator::make($request->all(), [
            'name' => 'max:100',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $wolfPack->update($validator->validated());
        return response()->json(new WolfPackResource($wolfPack));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     * @throws \Throwable
     */
    public function destroy($id)
    {
        $wolfPack = WolfPack::findOrFail($id);
        return response()->json(['success' => $wolfPack->deleteOrFail()]);
    }
}
