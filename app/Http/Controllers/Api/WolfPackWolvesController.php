<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\WolfPack as WolfPackResource;
use App\Models\Wolf;
use App\Models\WolfPack;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class WolfPackWolvesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index($wolfpack)
    {
        $wolfPack = WolfPack::findOrFail($wolfpack);
        return response()->json(new WolfPackResource($wolfPack));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param $wolfpack
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request, $wolfpack)
    {
        $wolfPack = WolfPack::findOrFail($wolfpack);

        $validator = Validator::make($request->all(), [
            'wolf' => 'numeric',
            'wolves' => 'array',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $validatedData = $validator->validated();
        $wolves = [];
        if(isset($validatedData['wolves'])){
            $wolves = array_map(function($wolfId) { return (int) $wolfId; }, $validatedData['wolves']);
        } elseif(isset($validatedData['wolf'])){
            $wolves[] = (int)$validatedData['wolf'];
        }
        $wolfPack->wolves()->sync($wolves);
        return response()->json(new WolfPackResource($wolfPack));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param $wolfpack
     * @param $wolf
     * @return JsonResponse
     */
    public function destroy($wolfpack, $wolf)
    {
        $wolfPack = WolfPack::findOrFail($wolfpack);
        $foundWolf = Wolf::findOrFail($wolf);
        $result = false;
        if($foundWolf instanceof Wolf){
            $wolfPack->wolves()->detach($foundWolf->id);
            $result = true;
        }
        return response()->json([
            'success' => $result
        ]);
    }
}
