<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wolf extends Model
{
    use HasFactory;

    protected $table = 'wolves';
    protected $connection = 'sqlite';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'firstname',
        'lastname',
        'email',
        'gender',
        'birthdate',
        'lat',
        'lon'
    ];

    public function wolfPacks()
    {
        return $this->belongsToMany(WolfPack::class, 'wolf_pack_wolf');
    }
}
