<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WolfPack extends Model
{
    use HasFactory;

    protected $table = 'wolf_packs';
    protected $connection = 'sqlite';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
    ];

    /**
     * Use the hidden array, to hide certain fields (globally)
     * @var array
     */
    protected $hidden = [];

    public function wolves()
    {
        return $this->belongsToMany(Wolf::class, 'wolf_pack_wolf');
    }

}
