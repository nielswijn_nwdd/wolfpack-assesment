<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWolfPackWolf extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wolf_pack_wolf', function (Blueprint $table) {
            $table->integer('wolf_id')->unsigned()->index();
            $table->foreign('wolf_id')->references('id')
                ->on('wolves')
                ->onDelete('cascade');
            $table->integer('wolf_pack_id')->unsigned()->index();
            $table->foreign('wolf_pack_id')->references('id')
                ->on('wolf_packs')
                ->onDelete('cascade');
            $table->primary(['wolf_id', 'wolf_pack_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wolf_pack_wolf');
    }
}
