<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWolvesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wolves', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('firstname', 100);
            $table->string('lastname', 100);
            $table->enum('gender', ['man', 'woman', 'undefined']);
            $table->string('email', 255)->nullable(true);
            $table->date('birthdate')->nullable(true);
            $table->float('lat')->nullable(true);
            $table->float('lon')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wolves');
    }
}
