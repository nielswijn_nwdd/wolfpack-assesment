<?php

namespace Database\Seeders;

use App\Models\Wolf;
use App\Models\WolfPack;
use Illuminate\Database\Seeder;

class WolfPackWolfSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $wolves = Wolf::all();
        WolfPack::all()->each(function($pack) use ($wolves) {
            $randWolvesCount = random_int(1, 10);
            $randomWolves = $wolves->take($randWolvesCount)->pluck('id');
            $pack->wolves()->sync($randomWolves);
        });

    }
}
