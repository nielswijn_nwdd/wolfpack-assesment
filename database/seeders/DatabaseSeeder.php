<?php

namespace Database\Seeders;

use App\Models\WolfPack;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            WolfPackSeeder::class,
            WolfSeeder::class,
            WolfPackWolfSeeder::class
        ]);
    }
}
