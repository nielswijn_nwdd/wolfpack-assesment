<?php

namespace Database\Seeders;

use Faker;
use Illuminate\Database\Seeder;

class WolfSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Wolf::factory(20)->create();
    }
}
