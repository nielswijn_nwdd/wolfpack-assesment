<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class WolfPackSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\WolfPack::factory(5)->create();
    }
}
