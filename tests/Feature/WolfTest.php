<?php

namespace Tests\Feature;

use App\Models\Wolf;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class WolfTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_endpoint()
    {
        $response = $this->get('/api/wolf');

        $response->assertStatus(200);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_endpoint_post()
    {
        $response = $this->postJson('/api/wolf', [
            "firstname" => "Niels",
            "lastname" => "Wijn",
            "gender" => "woman",
        ]);
        $response->assertStatus(200);
        $obj = json_decode($response->getContent());

        $response = $this->delete(sprintf('/api/wolf/%s', $obj->id));
        $response->assertStatus(200);
    }


    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_endpoint_post_invalid()
    {
        $response = $this->postJson('/api/wolf', [
            "firstname" => "Niels",
            "lastname" => "Wijn",
            "email" => "testtest.nl",
            "gender" => "man",
            "lat" => "",
            "lon" => ""
        ]);
        $response->assertStatus(200);
        $response->assertExactJson([
            "email" => ["The email must be a valid email address."],
            "lat" => ["The lat must be a number."],
            "lon" => ["The lon must be a number."]
        ]);
    }
}
