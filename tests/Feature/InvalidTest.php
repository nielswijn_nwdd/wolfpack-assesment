<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class InvalidTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_invalid_api()
    {
        $response = $this->get('/api/wolves');
        $response->assertStatus(404);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_invalid_api_post()
    {
        $response = $this->postJson('/api/wolfgeopoint', [
            ''
        ]);
        $response->assertStatus(405);
    }
}
